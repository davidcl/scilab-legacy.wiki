FROM docker.io/pandoc/core:latest-alpine

RUN apk add --upgrade python3 py3-pandocfilters
RUN ln -s /usr/bin/python3 /usr/bin/python
