#!/usr/bin/env sh

set -e

# first stage, from raw html to markdown
cp -alf wiki/* markdown/
find markdown -iname "*.html" -type f -print -exec sh -c 'pandoc "${0}" -f html-native_divs-native_spans -t markdown+grid_tables --filter pandoc_filter.py -o "${0%.html}.md"' {} \; -delete
# update link to internal page md
cd markdown
for f in *.md;
do
    name=$(basename "$f" .md)
    sed -i "s/\.\/$name.html/$name/g" ./*.md
done
