#!/usr/bin/env python3

"""
pandoc_filter.py
Pandoc filter to cleanup the wiki HTML pages before exporting them to markdown
"""

import sys
import json
from pandocfilters import Link, Header, walk, toJSONFilters


def _deepfind(x, search):
    stack = [x]
    while stack:
        node = stack.pop()
        if node == search:
            return True

        if isinstance(node, list):
            stack = stack + node
        elif isinstance(node, dict):
            stack = stack + [node.values()]
    return False


def _print(value):
    sys.stderr.write(json.dumps(value) + "\n")


def content(key, value, format, meta):
    if key == "Table":
        if value and len(value) > 4 and value[4] and value[4][0] and value[4][0][0] and value[4][0][0][0]:
            key = value[4][0][0][0]["t"]
            value = value[4][0][0][0]["c"]
        else:
            return None
    else:
        return None

    if key == "Plain":
        if value and value[0]:
            key = value[0]["t"]
            value = value[0]["c"]
        else:
            return None
    else:
        return None

    if key == "Image":
        if value and value[2] and value[2][0] == "logo.png":
            return []

    return None


def delink(key, value, format, meta):
    if key == "Link":
        if value and len(value) == 3:
            return Link(["", [], []], value[1], value[2])
    return None


def despan(key, value, format, meta):
    if key == "Span":
        if value and value[0] and value[0][0].startswith("line"):
            return []
        if value and value[0] and value[0][0] in ["bottom"]:
            return []
    return None

def debacklink(key, value, format, meta):
    if key == "BulletList":
        if value and value[0] and value[0][0]:
            key = value[0][0]["t"]
            value = value[0][0]["c"]
        else:
            return None
    else:
        return None

    if key == "Plain":
        if value and value[0] and "t" in value[0] and "c" in value[0]:
            key = value[0]["t"]
            value = value[0]["c"]
        else:
            return None
    else:
        return None

    if key == "Span":
        if value[0][1][0] == "backlink":
            return []

    return None


def deanchor(key, value, format, meta):
    if key == "Header":
        return Header(value[0], ["", [], []], value[2])
    return None


def dediv(key, value, format, meta):
    if key == "Div":
        if value and value[0][0] in ["page", "content"]:
            sys.stderr.write(json.dumps(value[1][0]) + '\n')
            return walk(value[1][0], actions, format, meta)
    return None


actions = [content, despan, deanchor, delink, debacklink]

if __name__ == "__main__":
    toJSONFilters(actions)
