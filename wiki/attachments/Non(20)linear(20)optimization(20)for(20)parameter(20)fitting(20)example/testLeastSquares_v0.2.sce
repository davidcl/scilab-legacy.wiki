// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


///////////////////////////////////////////////////////
//
// 1. The problem

function dy = myModel ( t , y , a , b )
   // The right-hand side of the Ordinary Differential Equation.
    dy(1) = -a*y(2) + y(1) + t^2 + 6*t + b 
    dy(2) = b*y(1) - a*y(2) + 4*t + (a+b)*(1-t^2) 
endfunction 


///////////////////////////////////////////////////////
//
// 2. Non linear least squares

function f = myDifferences ( k ) 
    // Returns the difference between the simulated differential 
    // equation and the experimental data.
    global MYDATA
    t = MYDATA.t
    y_exp = MYDATA.y_exp
    a = k(1) 
    b = k(2) 
    y0 = y_exp(1,:)
    t0 = 0
    y_calc=ode(y0',t0,t,list(myModel,a,b)) 
    diffmat = y_calc' - y_exp
    // Make a column vector
    f = diffmat(:)
	MYDATA.funeval = MYDATA.funeval + 1
endfunction 

function val = L_Squares ( k ) 
    // Computes the sum of squares of the differences.
    f = myDifferences ( k ) 
    val = sum(f.^2)
endfunction 

//
// Experimental data 
t = [0 1 2 3 4 5 6]'; 
y_exp(:,1) = [-1 2 11 26 47 74 107]'; 
y_exp(:,2) = [ 1 3 09 19 33 51 73]'; 
//
// Store data for future use
global MYDATA;
MYDATA.t = t;
MYDATA.y_exp = y_exp;
MYDATA.funeval = 0;

// 
// Initial guess
a = 0.1; 
b = 0.4; 
x0 = [a;b]; 

//
// Expected solution
xstar = [2;3];
fstar = L_Squares ( xstar ) 

///////////////////////////////////////////////////////
//
// 3. fminsearch

options = optimset("TolFun",1D-02,"TolX",1.D-02,"MaxFunEvals",1.0D+06,"MaxIter",1.0D+06); 
[xopt,fopt,flag,details] = fminsearch(L_Squares,x0,options)

///////////////////////////////////////////////////////
//
// 4. optim

function [f, g, ind] = modelCost (x, ind)
  f = L_Squares ( x )
  g = derivative ( L_Squares , x )
endfunction

[ fopt , xopt , gopt ] = optim ( modelCost , x0 )

///////////////////////////////////////////////////////
//
// 5. leastsq

[fopt,xopt,gopt]=leastsq(myDifferences, x0)

///////////////////////////////////////////////////////
//
// 6. lsqrsolve

function f = myDiffLsqrsolve ( x , m )
  f = myDifferences ( x ) 
endfunction

y0 = myDifferences ( x0 )
m = size(y0,"*")
[xopt,diffopt]=lsqrsolve(x0,myDiffLsqrsolve,m)
fopt = sum(diffopt.^2)


///////////////////////////////////////////////////////
//
// 7. Compare number of function evaluations

function printsummary(solvername,fopt,xopt,xstar,funeval)
  mprintf("%s:\n",solvername);
  mprintf("  Function evaluations: %d\n", funeval);
  mprintf("  Function value: %e\n", fopt);
  mprintf("  X: [%e,%e]\n", xopt(1), xopt(2));
  xopt=xopt(:)
  mprintf("  Absolute error on X: %e\n", norm(xopt-xstar));
endfunction
//
MYDATA.funeval = 0;
j = 0;
options = optimset("TolFun",1D-02,"TolX",1.D-02,"MaxFunEvals",1.0D+06,"MaxIter",1.0D+06); 
[xopt,fopt,flag,details] = fminsearch(L_Squares,x0,options);
j = j+1;
algos(j,1) = MYDATA.funeval;
algos(j,2) = fopt;
algos(j,3) = norm(xopt(:)-xstar);
printsummary("fminsearch",fopt,xopt,xstar,MYDATA.funeval);
//
MYDATA.funeval = 0;
[ fopt , xopt , gopt ] = optim ( modelCost , x0 );
j = j+1;
algos(j,1) = MYDATA.funeval;
algos(j,2) = fopt;
algos(j,3) = norm(xopt(:)-xstar);
printsummary("optim",fopt,xopt,xstar,MYDATA.funeval);
//
MYDATA.funeval = 0;
[fopt,xopt,gopt]=leastsq(myDifferences, x0);
j = j+1;
algos(j,1) = MYDATA.funeval;
algos(j,2) = fopt;
algos(j,3) = norm(xopt(:)-xstar);
printsummary("leastsq",fopt,xopt,xstar,MYDATA.funeval);
//
MYDATA.funeval = 0;
[xopt,diffopt,info]=lsqrsolve(x0,myDiffLsqrsolve,m);
fopt = sum(diffopt.^2);
j = j+1;
algos(j,1) = MYDATA.funeval;
algos(j,2) = fopt;
algos(j,3) = norm(xopt(:)-xstar);
printsummary("lsqrsolve",fopt,xopt,xstar,MYDATA.funeval);

// Comparison
algo_names = ["fminsearch" "optim" "leastsq" "lsqrsolve"];
for j = 1 : 4
   mprintf("%-15s %4d %e %e\n",algo_names(j),..
       algos(j,1),algos(j,2),algos(j,3));
end

