// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
//
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
//
// TP du 21 Fevrier 2013
// Statistiques

////////////////////////////////////////////////////////////////
// Estimation de la moyenne

//
// Experience A
// Distribution de la moyenne empirique d'une variable exponentielle.
//
// mu : moyenne de la variable exponentielle
// M : moyenne de la variable exponentielle (exacte)
// V : variance de la variable exponentielle (exacte)
// Nsample : nombre de repetitions de l'experience
// n : nombre de realisations de la variable aleatoire
// X : matrice de taille Nsample-par-n, 
//     realisations de la variable aleatoire
// Mn : matrice de taille Nsample-par-1, moyenne empirique
//
// Version "naive":
mu=12;
M=mu;
V=mu^2;
Nsample=10;
n=2;
X=distfun_exprnd(mu,Nsample,n);
mprintf("n=%d\n",n);
mprintf("E(Mn)=%f, V(Mn)=%f\n",M,V/n);
Mn=zeros(Nsample,1);
for iRow=1:Nsample
    for iCol=1:n
        Mn(iRow)=Mn(iRow)+X(iRow,iCol);
    end
    Mn(iRow)=Mn(iRow)/n;
end
disp(Mn)
// Version vectorisee 1:
Mn=sum(X,"c")/n;
disp(Mn)
// Version vectorisee 2:
Mn=mean(X,"c");
disp(Mn)
mprintf("#%d, Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
i, mean(Mn),variance(Mn));
//
// Version vectorisee:
mu=12;
[M,V] = distfun_expstat(mu);
Nsample=10000;
//
mprintf("Variable Exponentielle (mu=12)\n");
for n=[1 2 4 8]
    mprintf("n=%d\n",n);
    mprintf("E(Mn)=%f, V(Mn)=%f\n",M,V/n);
    for i=1:5
        X=distfun_exprnd(mu,Nsample,n);
        // Version 1 : 
        // Mn=sum(X,"c")/n;
        // Version 2 : 
        Mn=mean(X,"c");
        mprintf("#%d, Mean(Mn)=%f, Variance(Mn)=%f\n",.. 
        i, mean(Mn),variance(Mn));
    end
end

//
// Experience B
// Distribution de la moyenne empirique d'une variable exponentielle.
//
mu=12;
Nsample=1000;
[M,V]=distfun_expstat(mu);
x=linspace(0,100,100);
scf();
//
n=1;
X = distfun_exprnd(mu,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,1);
histplot(20,Mn)
y=distfun_normpdf(x,M,sqrt(V/n));
plot(x,y,"r-");
xtitle("Sample Mean - n=1","M","Frequency");
legend(["Data","Normal PDF"]);
//
n=2;
X = distfun_exprnd(mu,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,2);
histplot(20,Mn)
y=distfun_normpdf(x,M,sqrt(V/n));
plot(x,y,"r-");
xtitle("Sample Mean - n=2","M","Frequency");
legend(["Data","Normal PDF"]);
//
n=4;
X = distfun_exprnd(mu,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,3);
histplot(20,Mn)
y=distfun_normpdf(x,M,sqrt(V/n));
plot(x,y,"r-");
xtitle("Sample Mean - n=4","M","Frequency");
legend(["Data","Normal PDF"]);
//
n=8;
X = distfun_exprnd(mu,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,4);
histplot(20,Mn)
y=distfun_normpdf(x,M,sqrt(V/n));
plot(x,y,"r-");
xtitle("Sample Mean - n=8","M","Frequency");
legend(["Data","Normal PDF"]);

//
// Experience C
// Distribution de la moyenne empirique d'une variable normale.
//
mu=0;
sigma=1;
Nsample=1000;
x=linspace(-4,4,100);
scf();
//
n=1;
X = distfun_normrnd(mu,sigma,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,1);
histplot(20,Mn)
y=distfun_normpdf(x,mu,sigma/sqrt(n));
plot(x,y,"r-");
xtitle("n=1","M","Frequency");
legend(["Data","PDF"]);
//
n=2;
X = distfun_normrnd(mu,sigma,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,2);
histplot(20,Mn)
y=distfun_normpdf(x,mu,sigma/sqrt(n));
plot(x,y,"r-");
xtitle("n=2","M","Frequency");
legend(["Data","PDF"]);
//
n=4;
X = distfun_normrnd(mu,sigma,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,3);
histplot(20,Mn)
y=distfun_normpdf(x,mu,sigma/sqrt(n));
plot(x,y,"r-");
xtitle("n=4","M","Frequency");
legend(["Data","PDF"]);
//
n=8;
X = distfun_normrnd(mu,sigma,Nsample,n);
Mn=mean(X,"c");
subplot(2,2,4);
histplot(20,Mn)
y=distfun_normpdf(x,mu,sigma/sqrt(n));
plot(x,y,"r-");
xtitle("n=8","M","Frequency");
legend(["Data","PDF"]);
//
// Experience D
// Evolution de la PDF de la moyenne empirique 
// de n variables normales standard.
//
mu=0;
sigma=1;
x=linspace(-4,4,100);
y1=distfun_normpdf(x,mu,sigma/sqrt(1));
y2=distfun_normpdf(x,mu,sigma/sqrt(2));
y4=distfun_normpdf(x,mu,sigma/sqrt(4));
y10=distfun_normpdf(x,mu,sigma/sqrt(10));
scf();
plot(x,y1,"b-");
plot(x,y2,"r-");
plot(x,y4,"g-");
plot(x,y10,"k-");
xtitle("Distribution of the sample mean","M","Density");
legend(["n=1","n=2","n=4","n=10"]);

////////////////////////////////////////////////////////////////
//
// Estimation d'un intervalle de confiance sur la moyenne.
//

//
// Experience A
// Estimation par intervalle de la moyenne 
// d'une loi Log Normale Y = exp(X)
//
n = 100; // taille echantillon
mu = 2;
sigma = 1;
mux=distfun_lognstat(mu,sigma); // esperance de X
X = distfun_lognrnd(mu,sigma,n,1); // Echantillon X
Mn = mean(X); // moyenne empirique
Sn2 = variance(X,"r",1); // variance empirique (biaisee)
level=0.05; // =1-0.95
al=level/2;
// Quand n n'est pas tres grand:
q = distfun_tinv(al,n-1,%f); // quantile 0.025 ~ 1.98
// Quand n et grand:
//q = distfun_norminv(al,0,1,%f); // quantile 0.025 ~ 1.96
borne = q * sqrt(Sn2/(n-1));
low=Mn-borne;
up=Mn+borne;
mprintf("Moyenne exacte = %f\n",mux);
mprintf("Moyenne empirique = %f\n",Mn);
mprintf("Intervalle a 0.95%%: [%f,%f]\n",low,up);
//
// Experience B
// Verification.
//
Nsample=10000;
X = distfun_lognrnd(mu,sigma,n,Nsample); // Echantillon X
Mn = mean(X,"r");
Sn2 = variance(X,"r",1); // variance empirique
borne = q * sqrt(Sn2/(n-1));
low=Mn-borne;
up=Mn+borne;
x=linspace(5,25,50);
scf();
histplot(x,low,style=1);
histplot(x,up,style=2);
plot([mux,mux],[0,0.3],"r-");
legend(["Lower Bound","Upper Bound","E(X)"]);
xtitle("Invervalle de confiance a 95% - X~Log-Normale",..
"Mean","Frequency")
//
// Calcul de P(I contains mux)
//
i=find(mux>low&mux<up);
nInBounds=size(i,"*");
pInBounds=nInBounds/Nsample;
mprintf("P(I contains E(X))=%f\n",pInBounds);

////////////////////////////////////////////////////////////////
//
// Estimation de la variance
//

// Experience A
// Calcul de la variance non biaisee
mu=5;
[M,V] = distfun_expstat(mu);
Nsample=10;
n=2;
X=distfun_exprnd(mu,Nsample,n);

// Version naive
Mn=mean(X,"c");
Sn=zeros(Nsample,1);
for iRow=1:Nsample
    for iCol=1:n
        Sn(iRow)=Sn(iRow)+(X(iRow,iCol)-Mn(iRow))^2;
    end
    Sn(iRow)=Sn(iRow)/(n-1);
end
disp(Sn)

// Version vectorisee:
Sn=variance(X,"c")
disp(Sn)
//
mu=5;
[M,V] = distfun_expstat(mu);
Nsample=10000;
mprintf("Variable Exponentielle (mu=%f)\n",mu);
for n=[2 4 8 16]
    mprintf("n=%d\n",n);
    mprintf("E(Sn)=%f\n",V);
    for i=1:5
        X=distfun_exprnd(mu,Nsample,n);
        Sn=variance(X,"c");
        mprintf("#%d, Mean(Sn)=%f\n",.. 
        i, mean(Sn));
    end
end

//
// Experience B
// Variance biaisee / non biaisee
// "Correction de Bessel"
//
mu=5;
mprintf("Variable Exponentielle (mu=%f)\n",mu);
[M,V] = distfun_expstat(mu);
mprintf("E(X)=%f\n",M);
mprintf("V(X)=%f\n",V);
mprintf("Esperance(Variance non biaisee):%f\n",V);
n=2;
Nsample=10000;
X=distfun_exprnd(mu,Nsample,n);
//
// Non biaisee
S=variance(X,"c",0);
mprintf("Moyenne(Variance non biaisee):%f\n",mean(S));
//
// Biaisee
Vb=V*(n-1)/n;
mprintf("Esperance(Variance biaisee):%f\n",Vb);
Sb=variance(X,"c",1);
mprintf("Moyenne(Variance biaisee):%f\n",mean(Sb));

// Experience C
// Difference entre la variance biaisee et la 
// variance non biaisee
mu=1;
Nsample=1000;
Sn=[];
Snb=[];
X=distfun_exprnd(mu,Nsample,1);
V=variance(X,"r");
for n=2:Nsample
    Sn(n)=variance(X(1:n),"r",0);
    Snb(n)=variance(X(1:n),"r",1);
end
h=scf();
plot(1:Nsample,Sn,"r-");
plot(1:Nsample,Snb,"b-");
plot([1,Nsample],[V,V],"k-");
h.children.log_flags="lnn";
xtitle("Difference V. biaisee - non-biaisee",..
"n","Variance empirique");
legend(["V. non biaisee","V. biaisee","Var(X)"],"in_upper_left");

// Distribution de la variance empirique
TODO

////////////////////////////////////////////////////////////////
//
// Estimation d'une probabilite de depassement
//

// Experience A
// Probabilite de depassement d'un seuil 
// d'une variable log-normale
mu=2;
sigma=3;
seuil=1.e4;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
mprintf("seuil:%e\n",seuil);
// Calcul exact
pfExacte=distfun_logncdf(seuil,mu,sigma,%f);
mprintf("Pf (exact):%e\n",pfExacte);
// Estimation Monte-Carlo
Nsample=100000;
mprintf("Nombre de simulations:%d\n",Nsample);
X=distfun_lognrnd(mu,sigma,Nsample,1);
i=find(X>seuil);
nfail=size(i,"*");
mprintf("Nombre de depassements:%d\n",nfail);
pf=nfail/Nsample;
mprintf("Pf (estimation):%e\n",pf);

// Experience B
// Calcul d'un intervalle de confiance à 1-alpha=95% pour Pf
// alpha=0.05
al=0.05;
q = al/2.;
f = distfun_norminv(q,0,1,%f);
low = pf - f * sqrt(pf*(1-pf)/Nsample);
up = pf + f * sqrt(pf*(1-pf)/Nsample);
mprintf("95%% Int. de Conf.:[%e,%e]\n" , low,up);

// Experience C
// Comparaison entre Monte-Carlo simple et 
// la transformation arcsin(sqrt(p))
Nsample=1000;
al=0.05;
q = al/2.;
f = distfun_norminv(q,0,1,%f);
n=20;
ptab=10.^linspace(-3,-2,n);
ptab=ptab';
lowTCL=[];
upTCL=[];
lowAS=[];
upAS=[];
for i=1:n
    p=ptab(i);
    // Via le TCL
    s=f*sqrt(p*(1-p)/Nsample);
    lowTCL(i)=max(0,p-s);
    upTCL(i)=p+s;
    // Via la transformation arcsin(p)
    t=asin(sqrt(p));
    s=f/(2*sqrt(Nsample));
    lowAS(i)=sin(max(0,t-s))^2;
    upAS(i)=sin(t+s)^2;
end
h=scf();
plot(ptab,lowTCL,"b-");
plot(ptab,lowAS,"r-");
plot(ptab,ptab,"k-");
plot(ptab,upTCL,"b-");
plot(ptab,upAS,"r-");
xtitle("1000 realisations","p","Intervalle de confiance à 95%");
legend(["TCL","Arcsin","pn"],"in_upper_left");

// Experience D
// Distribution d'une probabilite de defaillance empirique
n=200;
Nrepeat=500;
mu=3;
seuil=8;
// Calcul exact
pfExacte=distfun_expcdf(seuil,mu,%f);
mprintf("Pf (exact):%e\n",pfExacte);
// Estimation Monte-Carlo
X=distfun_exprnd(mu,n,Nrepeat);
y=zeros(X);
y(X>seuil)=1;
b=sum(y,"r");
pf=b/n;
nbclasses=ceil(log2(Nrepeat)+1);
v=pfExacte*(1-pfExacte)*n;
sigma=sqrt(v);
x=linspace(n*pfExacte-3*sigma,n*pfExacte+3*sigma,100);
x=floor(x);
x=max(0,x);
x=unique(x);
y=distfun_binopdf(x,n,pfExacte);
ymax=max(y);
//
scf();
histplot(unique(pf*n),pf*n);
plot([pfExacte*n,pfExacte*n],[0,ymax],"r-");
plot(x,y,"b*-")
legend(["Donnees","Exact","Binomial"]);
strtitle=msprintf("%d realisations, %d repetitions",n,Nrepeat)
xtitle(strtitle,"Pf*n","Frequence");

////////////////////////////////////////////////////////////////
//
// Intervalle de confiance de la moyenne d'une variable normale
//
// Experience A (variance connue)
mu=2;
sigma=3;
mprintf("Variable normale, mu=%f, sigma=%f\n",..
mu,sigma);
Nsample=10000;
level=0.05; // 1-0.95
mprintf("Number of samples:%d, level=%f",..
Nsample,level);
X=distfun_normrnd(mu,sigma,Nsample,1);
Mn=mean(X);
al=level/2;
z=distfun_norminv(al,0,1,%f);
delta=z*sigma/sqrt(Nsample);
low=Mn-delta;
up=Mn+delta;
mprintf("Moyenne empirique: %f\n",Mn)
mprintf("Int. Conf.:[%f,%f]\n",low,up)

// Experience B (variance inconnue)
Sn=variance(X,"r",1);
z=distfun_tinv(al,Nsample-1,%f);
delta=z*sqrt(Sn/(Nsample-1));
low=Mn-delta;
up=Mn+delta;
mprintf("Int. Conf.:[%f,%f]\n",low,up)

// Experience C
// Distribution de Q=n*S/sigma^2
// quand X est normal
mu=0;
sigma=1;
n=5;
Nsample=10000;
X=distfun_normrnd(mu,sigma,Nsample,n);
S=variance(X,"c",1);
Q=n*S/sigma^2;
//
h=scf();
x=linspace(0,10,50);
x($+1)=%inf;
histplot(x,Q);
x=linspace(0,10,100);
y = distfun_chi2pdf(x,n-1);
plot(x,y);
xtitle("Distribution de Q","Q","Frequence");
legend(["Data","$\chi^2_{4}$"]);

// Experience D
// Distribution de T=(M-mu)/sqrt(S/(n-1))
// quand X est normal
mu=0;
sigma=1;
n=5;
Nsample=10000;
X=distfun_normrnd(mu,sigma,Nsample,n);
M=mean(X,"c");
S=variance(X,"c",1);
T=(M-mu)./(sqrt(S/(n-1)));
scf();
x=linspace(-5,5,50);
histplot(x,T);
x=linspace(-5,5,100);
y=distfun_tpdf(x,n-1);
plot(x,y,"r-");
xtitle("Distribution de T","T","Frequence");
legend(["Data","Densite de T4"]);

////////////////////////////////////////////////////////////////
// Estimation d'un quantile

// Experience A
// Calcul d'un quantile d'une variable log-normale
// quantile en queue basse de fonction de repartition.
// Une probabilite alpha est donnee, et 
// on cherche x tel que P(X<x)=alpha.

mu=2;
sigma=3;
al=0.1;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
mprintf("alpha:%e\n",al);
// Calcul exact
xExact=distfun_logninv(al,mu,sigma);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=100000;
mprintf("Nombre de simulations:%d\n",Nsample);
X=distfun_lognrnd(mu,sigma,Nsample,1);
X=gsort(X,"g","i");
i=floor(Nsample*al);
x=X(i);
mprintf("x (estimation):%e\n",x);

//
// Experience B
// Estimation d'un quantile en queue haute.
// Une probabilite alpha est donnee, et 
// on cherche x tel que P(X>x)=alpha.
//
mu=2;
sigma=3;
al=1.e-3;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
mprintf("alpha:%e\n",al);
// Calcul exact
xExact=distfun_logninv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=100000;
mprintf("Nombre de simulations:%d\n",Nsample);
X=distfun_lognrnd(mu,sigma,Nsample,1);
X=gsort(X,"g","d");
i=floor(Nsample*al);
x=X(i);
mprintf("x (estimation):%e\n",x);

//
// Experience C
// Estimation d'un quantile en queue haute.
// Une probabilite alpha est donnee, et 
// on cherche x tel que P(X>x)=alpha.
//
mu=2;
sigma=3;
al=1.e-3;
mprintf("Variable Log-normale\n");
mprintf("mu=%f\n",mu);
mprintf("sigma:%e\n",sigma);
// Calcul exact
xExact=distfun_logninv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
x=[];
// Estimation Monte-Carlo
Ntab=2 .^(10:20);
for Nsample=Ntab
    X=distfun_lognrnd(mu,sigma,Nsample,1);
    X=gsort(X,"g","d");
    i=floor(Nsample*al);
    if (i>0) then
        x($+1)=X(i);
    else
        x($+1)=0;
    end
end
h=scf();
plot(Ntab,x,"rx");
h.children.log_flags="lnn";
plot([2^10,2^20],[xExact,xExact],"b-");
xtitle("Convergence d''un quantile alpha=1.e-3","n","x");
legend(["Empirique","Exact"]);

//
// Experience D
// Estimation d'un quantile en queue haute.
// X est uniforme dans [0,1].
//
al=0.05; // 1-0.95
mprintf("Variable Uniforme [0,1]\n");
mprintf("Quantile a 1-%f\n",al);
// Calcul exact
xExact=distfun_unifinv(al,0,1,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=200;
nRepeat=10000;
X=distfun_unifrnd(0,1,nRepeat,Nsample);
X=gsort(X,"c","d");
i=floor(Nsample*al);
x=X(:,i);
//
h=scf();
histplot(20,x);
plot([xExact,xExact],[0,30]);
plot(s,y,"r-");
xtitle("10000 quantiles a 95% sur 200 realisations",..
"Quantile","Frequence");
legend(["Data","Exact"]);

//
// Experience E
// Estimation d'un quantile en queue haute.
// X est normale(4,7)
//
al=0.05; // 1-0.95
mprintf("Quantile a 1-%f\n",al);
mu=4;
sigma=7;
// Calcul exact
xExact=distfun_norminv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=200;
nRepeat=10000;
X=distfun_normrnd(mu,sigma,nRepeat,Nsample);
X=gsort(X,"c","d");
i=floor(Nsample*al);
x=X(:,i);
//
// Distribution of the sample quantile
mprintf("Moyenne Empirique(X)=%f\n",mean(x))
mprintf("E(X)=%f\n",xExact)
mprintf("Variance Empirique(X)=%f\n",variance(x))
y=distfun_normpdf(xExact,mu,sigma);
V=al*(1-al)/(y^2)/Nsample;
mprintf("V(X)=%f\n",V)
//
t=linspace(12,19,20);
h=scf();
histplot(t,x);
plot([xExact,xExact],[0,0.4]);
s=linspace(xExact-3*sqrt(V),xExact+3*sqrt(V),100);
y=distfun_normpdf(s,xExact,sqrt(V));
plot(s,y,"r-")
xtitle("10000 quantiles a 95% sur 200 realisations",..
"Quantile","Frequence");
legend(["Data","Exact","Asymp. Normal"]);

////////////////////////////////////////////////////////////////
//
// Quantile de Wilks
//
//
// Experience A
//
function r = wilks(alpha,bet,n)
    // Calcule le rang r, tel que :
    // P(Y(r)>y(alpha))>bet
    // avec y(alpha) le quantile de probabilite alpha, i.e.
    // P(Y<y(alpha))=alpha.
    // Si il n'y a pas assez de donnees, renvoit r=0.
    // Require: specfun, distfun
    if (n < specfun_log1p(-bet)/log(alpha)) then 
        r=0;
    else
        r = distfun_binoinv(bet,n,alpha)
        r = r + 1
    end
endfunction
// mediane (alpha=0.5)
wilks(0.5,0.5,100) // => 51 // confiance 0.5
wilks(0.5,0.95,100) // => 59 // confiance 0.95
// quantile 0.95, confiance 0.95
wilks(0.95,0.95,53) // => 0 // pas assez de donnees
wilks(0.95,0.95,59) // => 59 // la valeur extreme
wilks(0.95,0.95,124) // => 122 // l'avant derniere valeur
wilks(0.95,0.95,153) // => 150 ...

//
// Experience B
// Estimation d'un quantile en queue haute.
// X est uniforme dans [0,1].
//
stacksize("max");
al=0.05; // 1-0.95
mprintf("Variable Uniforme [0,1]\n");
mprintf("Quantile a 1-%f\n",al);
// Calcul exact
xExact=distfun_unifinv(al,0,1,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=200;
nRepeat=10000;
X=distfun_unifrnd(0,1,nRepeat,Nsample);
X=gsort(X,"c","d");
i=floor(Nsample*al);
x=X(:,i);
j=200-wilks(1-al,0.95,Nsample);
y=X(:,j);
//
s=linspace(0.9,1.,20);
h=scf();
histplot(s,x,style=1);
histplot(s,y,style=2);
plot([xExact,xExact],[0,30],"r-");
xtitle("10000 quantiles a 95% sur 200 realisations",..
"Quantile","Frequence");
legend(["Monte-Carlo","Wilks","Exact"],"in_upper_left");

////////////////////////////////////////////////////////////////
//
// Fonction de repartition empirique
//

// Experience A (loi normale)

n = 100; // taille de l'echantillon
X = distfun_normrnd(0,1,n,1); // echantillon X ~ N(0,1)
X = gsort(X,"g","i"); // on reordonne par valeurs croissantes
// Fonction de repartition aux points echantillon
p = distfun_normcdf(X,0,1);
scf();
plot(X,(1:n)/n,"b-"); // Fonction de repartition empirique
plot(X,p,"r-"); // Fonction de repartition
xtitle("Normale(0,1) - 100 realisations","x","P(X<x)");
legend(["CDF Empirique","CDF"],"in_upper_left");

// Experience B (loi exponentielle)
n = 100; // taille de l'echantillon
mu=5;
X = distfun_exprnd(mu,n,1); // echantillon X ~ Exp(mu)
X = gsort(X,"g","i");
p = distfun_expcdf(X,mu);
scf();
plot(X,(1:n)/n,"b-");
plot(X,p,"r-");
stitle=msprintf("Exp(%f) - %d realisations",mu,n);
xtitle(stitle,"x","P(X<x)");
legend(["CDF Empirique","CDF"],"in_lower_right");

// Experience C (convergence quand n augmente)
function plotExpEcdf(n,mu)
    X = distfun_exprnd(mu,n,1);
    X = gsort(X,"g","i");
    p = distfun_expcdf(X,mu);
    plot(X,(1:n)/n,"b-");
    plot(X,p,"r-");
    stitle=msprintf("Exp(%.2f) - %d realisations",mu,n);
    xtitle(stitle,"x","P(X<x)");
    legend(["CDF Empirique","CDF"],"in_lower_right");
endfunction

mu=5;
h=scf();
subplot(2,2,1);
plotExpEcdf(100,mu);
subplot(2,2,2);
plotExpEcdf(200,mu);
subplot(2,2,3);
plotExpEcdf(500,mu);
subplot(2,2,4);
plotExpEcdf(1000,mu);
h.children(1).data_bounds(2,1)=30;
h.children(2).data_bounds(2,1)=30;
h.children(3).data_bounds(2,1)=30;
h.children(4).data_bounds(2,1)=30;

////////////////////////////////////////////////////////////////
//
// QQ-Plot
//

// Experience A1
function [y,p] = quantileEmpirique(x)
    n = length(x);
    y = gsort(x,"g","i");
    p = [1:n] / (n+1);
endfunction
n=1000; // taille de l'echantillon
mu=1; 
sigma= 3;
x=distfun_normrnd(mu,sigma,1,n); // echantillon de la loi normale
[x,p]=quantileEmpirique(x); // quantiles et probabilite associes
y=distfun_norminv(p,mu,sigma); // quantiles de la loi normale
scf();
plot(y,x,"b-");
plot([y(250),y(750)],[x(250),x(750)],"r-");
xtitle("QQ Plot","Normal Quantile","Data Quantile");

// Experience A2
n=20;
mu=0; 
sigma= 1;
p=linspace(0.01,0.99,n);
y=distfun_norminv(p,mu,sigma);
scf();
plot(y,y,"bo");
plot(y,y,"r-");
xtitle("QQ Plot","Normal Quantiles","Normal Quantiles");
ymax=max(y);
for i=1:n
    plot([y(i),y(i)],[-ymax,ymax])
    plot([-ymax,ymax],[y(i),y(i)])
end

// Experience B (stixbox)
mu=1; 
sigma= 3;
//
scf();
k=0;
for n=[10 50 100 1000]
    k=k+1;
    subplot(2,2,k)
    x=distfun_normrnd(mu,sigma,1,n);
    x=gsort(x,"g","i");
    p=(1:n)/(n+1);
    y=distfun_norminv(p,mu,sigma);
    qqplot(y,x,"bo");
    i1=ceil(0.25*n);
    i3=ceil(0.75*n);
    plot([y(i1),y(i3)],[x(i1),x(i3)],"r-")
    strtitle=msprintf("n=%d",n);
    xtitle(strtitle,"Normal Quantile","Data Quantile");
end

// Experience C (Temperature du corps)
x=fscanfMat("normtemp.dat.txt");
// Retire la seconde colonne (le sexe)
x(:,2)=[];
n=size(x,"r");
m=size(x,"c");
mu=mean(x,"r"); 
sigma= sqrt(variance(x,"r"));
p=(1:n)'/(n+1);
strleg=["Temp. Corps (F)","Pulsations Cardiaques (batt/min)"];
nbclasses=ceil(log2(n)+1);
scf();
k=0;
for i=1:m
    y=distfun_norminv(p,mu(i),sigma(i));
    //
    k=k+1;
    subplot(m,2,k)
    qqplot(y,x(:,i),"b*-");
    plot(x(:,i),x(:,i),"r-")
    strtitle=msprintf("%s",strleg(i));
    xtitle(strtitle,"Normal Quantile","Data Quantile");
    //
    k=k+1;
    subplot(m,2,k)
    histplot(nbclasses,x(:,i));
    xtitle(strtitle,"X","Frequency");
end
//
// Remarque
// Run Sequence Plot.
// Sur la temperature du corps, les donnees 
// sont triees: les hommes, puis les femmes, 
// par ordre croissant. 
// D'ou la forme en deux "S".
// La moyenne et la variance des pulsations semblent 
// constantes.
scf();
subplot(1,2,1);
plot(1:n,x(:,1),"ro-");
xtitle("","Indice","Temp. Corps (F)")
subplot(1,2,2);
plot(1:n,x(:,2),"ro-");
xtitle("","Indice","Pulsations Cardiaques (batt/min)")
//
// Lag Plot
// Les temperatures sont triees.
// Les pulsations semblent aleatoires.
scf();
subplot(1,2,1);
plot(x(1:$-1,1),x(2:$,1),"ro");
xtitle("Temp. Corps (F)","X(i-1)","X(i)")
subplot(1,2,2);
plot(x(1:$-1,2),x(2:$,2),"ro");
xtitle("Pulsations Cardiaques (batt/min)","X(i-1)","X(i)")

// Experience D (Groupes francais et europeens)
idataset=23;
[x,txt]=getdata(idataset);
// Retire la colonne #1 (CA)
x(:,1)=[];
n=size(x,"r");
m=size(x,"c");
mu=mean(x,"r"); 
sigma= sqrt(variance(x,"r"));
p=(1:n)'/(n+1);
nbclasses=ceil(log2(n)+1);
strleg=["Nb. Salaries (*1000)","Revenu Net (MF)"];
scf();
k=0;
for i=1:m
    y=distfun_norminv(p,mu(i),sigma(i));
    //
    k=k+1;
    subplot(m,2,k)
    qqplot(y,x(:,i),"b*-");
    plot(x(:,i),x(:,i),"r-")
    strtitle=msprintf("%s",strleg(i));
    xtitle(strtitle,"Normal Quantile","Data Quantile");
    //
    k=k+1;
    subplot(m,2,k)
    histplot(nbclasses,x(:,i));
    xtitle(strtitle,"X","Frequency");
end

// Experience E (Variable uniforme)
n=10000;
x=distfun_unifrnd(0,1,n,1);
mu=mean(x); 
sigma= sqrt(variance(x));
p=linspace(0.01,0.99,100)';
q=quantile(x,p);
nbclasses=ceil(log2(n)+1);
y=distfun_norminv(p,mu,sigma);
//
scf();
subplot(1,2,1)
qqplot(y,q,"bo");
plot([y(25),y(75)],[q(25),q(75)],"r-")
xtitle("U(0,1) - n=10 000","Normal Quantile","Data Quantile");
subplot(1,2,2)
histplot(nbclasses,x);
xtitle("U(0,1) - n=10 000","X","Frequency");

// Experience F (Variable exponentielle)
n=10000;
x=distfun_exprnd(1,n,1);
mu=mean(x); 
sigma= sqrt(variance(x));
p=linspace(0.1,0.9,100)';
q=quantile(x,p);
nbclasses=ceil(log2(n)+1);
y=distfun_norminv(p,mu,sigma);
//
scf();
subplot(1,2,1)
qqplot(y,q,"bo");
plot([y(25),y(75)],[q(25),q(75)],"r-")
xtitle("Exp(1) - n=10 000","Normal Quantile","Data Quantile");
subplot(1,2,2)
histplot(nbclasses,x);
xtitle("Exp(1) - n=10 000","X","Frequency");

// Experience G (Variable Normale s1>s2)
n=10000;
x1=distfun_normrnd(0,1,n,1);
x2=distfun_normrnd(0,2,n,1);
p=linspace(0.1,0.9,100)';
q1=quantile(x1,p);
q2=quantile(x2,p);
//
scf();
subplot(1,2,1)
qqplot(q1,q2,"bo");
plot([q1(25),q1(75)],[q2(25),q2(75)],"r-")
xtitle("n=10 000","Normal(0,1)","Normal(0,2)");
subplot(1,2,2)
histplot(20,x1,style=1);
histplot(20,x2,style=2);
xtitle("n=10 000","X","Frequency");
legend(["Normal(0,1)","Normal(0,2)"]);

