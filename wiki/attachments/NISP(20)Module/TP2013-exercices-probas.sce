// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
//
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
//
// TP du 21 Fevrier 2013
// Probabilites

////////////////////////////////////////////////////////////////

// Loi Binomiale

// 
N=20;
pr=0.5;
x=1;
// Avec factorial:
c=factorial(N)/factorial(x)/factorial(N-x)
c*pr^x*pr^(N-x)
// Avec nchoosek:
specfun_nchoosek(N,x)*pr^x*pr^(N-x)
// Avec binopdf:
distfun_binopdf(x,N,pr)
x=10;
// Avec nchoosek:
specfun_nchoosek(N,x)*pr^x*pr^(N-x)
// Avec binopdf:
distfun_binopdf(x,N,pr)
//
scf();
N1 = 20;
x = 0:N1;
y1 = distfun_binopdf(x,N1,0.5);
plot(x,y1,"bo-")
N2 = 20;
x = 0:N2;
y2 = distfun_binopdf(x,N2,0.7);
plot(x,y2,"go-")
N3 = 40;
x = 0:N3;
y3 = distfun_binopdf(x,N3,0.5);
plot(x,y3,"ro-")
legend(["pr=0.5, N=20","pr=0.7, N=20","pr=0.5, N=40"]);
xtitle("Binomial PDF","x","P(x)")
//
// Let X be the number of non-defective items in the sample. 
// Then X has a binomial distribution, with parameter pr=0.8.
// To calculate the required probabilities, we can use the 
// distfun_binopdf function as follows.
s0 = distfun_binopdf(0,3,0.8)
s1 = distfun_binopdf(1,3,0.8)
s2 = distfun_binopdf(2,3,0.8)
s3 = distfun_binopdf(3,3,0.8)
s0+s1+s2+s3

////////////////////////////////////////////////////////////////

// Loi Uniforme
 
//
N = 1000;
a = 6;
b = 13;
// Esperance:
m = (a+b)/2
// Variance:
v = (b-a)^2/12
[M,V]=distfun_unifstat(a,b)
R = distfun_unifrnd(a,b,N,1);
mean(R)
variance(R)

//
// Make a plot of the actual distribution of the numbers
a = 6;
b = 13;
data = distfun_unifrnd(a,b,1,1000);
scf();
histplot(10,data)
x = linspace(a-1,b+1,1000);
y = distfun_unifpdf(x,a,b);
plot(x,y)
xtitle("Uniform random numbers","X","Density");
legend(["Empirical","PDF"]);

////////////////////////////////////////////////////////////////

// Loi Normale

// Plot the PDF (with exp)
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
y = exp(-(x-mu)^2/(2*sigma^2))/(sigma*sqrt(2*%pi));
plot(x,y,"r-")
xtitle("Densite de probabilite Normale - mu=5, sigma=7",..
"x","f(x)");

// Plot the PDF (with distfun_normpdf)
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
y = distfun_normpdf ( x , mu , sigma );
plot(x,y,"r-")
xtitle("Densite de probabilite Normale - mu=5, sigma=7",..
"x","f(x)");

// Plot the CDF
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
p = distfun_normcdf ( x , mu , sigma );
plot(x,p,"b-")
xtitle("Fonction Repartition Normale - mu=5, sigma=7",..
"x","$P(X\leq x)$");

////////////////////////////////////////////////////////////////

// Puissance dissipee par une resistance

mu=6
sigma=1
R=1/3
EW=(mu^2+sigma^2)/R
threshold=120
t=sqrt(threshold*R)
PW = distfun_normcdf(t,mu,sigma,%f)

// Variation du seuil
threshold = linspace(1,300,100);
t=sqrt(threshold*R);
PW = distfun_normcdf(t,mu,sigma,%f);
scf();
plot(threshold,PW)
xtitle("Puissance dissipee","Seuil","P(W>Seuil)");


////////////////////////////////////////////////////////////////

// Regle des 3 sigma

distfun_normcdf(1,0,1)-distfun_normcdf(-1,0,1)
distfun_normcdf(2,0,1)-distfun_normcdf(-2,0,1)
distfun_normcdf(3,0,1)-distfun_normcdf(-3,0,1)

////////////////////////////////////////////////////////////////

// Lien entre la loi de Poisson et la loi normale

lambda=[4. 16. 32. 10000.];
ny=2;
nx=2;
scf();
for i=1:nx
    for j=1:ny
        ij=(i-1)*ny + j;
        subplot(ny,nx,ij)
        mu=lambda(ij);
        sigma=sqrt(lambda(ij));
        xmin=max(mu-3*sigma,0);
        xmax=mu+3*sigma;
        x=linspace(xmin,xmax,100);
        xpoi=unique(floor(x));
        y=distfun_poisspdf(xpoi,lambda(ij));
        plot(xpoi,y,"ro");
        y=distfun_normpdf(x,mu,sigma);
        plot(x,y,"b-");
        xtitle("lambda="+string(lambda(ij)));
        legend(["Poisson","Normal"]);
    end
end

////////////////////////////////////////////////////////////////

// Theoreme limite central
a=-4;
b=2;
[M,V]=distfun_unifstat(a,b);
N=10000;
scf();
// 
k=1;
subplot(2,2,1);
R=distfun_unifrnd(a,b,N,k);
S=(sum(R,"c")-k*M)/(sqrt(k*V));
histplot(20,S);
x=linspace(-3,3,100);
y=distfun_normpdf(x,0,1);
plot(x,y,"b-")
xtitle("k=1","x","Density")
legend(["Data","Normal(0,1)"]);
//
k=2;
subplot(2,2,2);
R=distfun_unifrnd(a,b,N,k);
S=(sum(R,"c")-k*M)/(sqrt(k*V));
histplot(20,S);
x=linspace(-3,3,100);
y=distfun_normpdf(x,0,1);
plot(x,y,"b-")
xtitle("k=2","x","Density")
legend(["Data","Normal(0,1)"]);
//
k=4;
subplot(2,2,3);
R=distfun_unifrnd(a,b,N,k);
S=(sum(R,"c")-k*M)/(sqrt(k*V));
histplot(20,S);
x=linspace(-3,3,100);
y=distfun_normpdf(x,0,1);
plot(x,y,"b-")
xtitle("k=4","x","Density")
legend(["Data","Normal(0,1)"]);
//
k=8;
subplot(2,2,4);
R=distfun_unifrnd(a,b,N,k);
S=(sum(R,"c")-k*M)/(sqrt(k*V));
histplot(20,S);
x=linspace(-3,3,100);
y=distfun_normpdf(x,0,1);
plot(x,y,"b-")
xtitle("k=8","x","Density")
legend(["Data","Normal(0,1)"]);

////////////////////////////////////////////////////////////////

// Changement de loi Uniforme vers Exponentielle

N=10000;
mu=5;
U=distfun_unifrnd(0,1,N,1);
R=-mu*log(U);
scf();
histplot(20,R)
x=linspace(0,40,100);
y=distfun_exppdf(x,mu);
plot(x,y);
legend(["Random","Density"])
xtitle("Uniforme vers Exponentielle","R","Densite")





