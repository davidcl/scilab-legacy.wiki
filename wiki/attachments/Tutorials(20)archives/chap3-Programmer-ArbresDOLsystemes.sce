// Copyright (C) 2004-2011 - J.-Ph. Chancelier.  
// 
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
// 
// You should have received a copy of the GNU General Public
//  License along with this library; if not, write to the
//  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//  Boston, MA 02111-1307, USA.

// 
// Source: "Introduction à Scilab", Chancelier, 2004
// Chapitre 3: Programmer : Arbres et DOL-systemes
//
// With adaptations for Scilab v5, 2011, Michael Baudin

stacksize("max");

// Chapitre 3
// Programmer : Arbres et DOL-systemes

// 3.0.1 DOL-systemes

function [w]=Irewrite0(v)
    w=strsubst(v,'F','X+F+X'); // Regle : F 7! H+F+H. On protege les H nommes X pour l'instant.
    w=strsubst(w,'H','F-H-F'); // Regle : H 7! F-H-F.
    w=strsubst(w,'X','H'); // On renomme les X en H.
endfunction

function [w]=rewrite(macro,init,n)
    w=init; // init est l'axiome.
    for i=1:n
        w=macro(w);
    end // init On applique les re-ecriture
endfunction

w=rewrite(Irewrite0,'F',3)

// 3.0.2 Interpretation des cha^³nes
function [xs,ys]=calcul_segments(w,delta,l)
    delta = delta*%pi/180;
    Rm = [cos(delta),-sin(delta);sin(delta),cos(delta)]
    Rp = [1,-1;-1,1].*Rm;
    L=length(w);
    res=ones(4,L); // Matrice pour stocker les segments calcules
    count=1; // premier indice utilisable pour stocker un segment
    pt=[0;0];
    ddir=[0;1]; // Point et direction initiaux.
    for i=1:L
        select part(w,i) // Le i-ieme caractere de w.
        case '+' then
            ddir= Rp*ddir;
        case '-' then
            ddir= Rm*ddir;
        else
            newpt=pt + l*ddir;
            res(:,count)=[pt(1);newpt(1);pt(2);newpt(2)];
            pt = newpt;
            count=count+1;
        end
    end
    xs=res(1:2,1:count-1);
    ys=res(3:4,1:count-1);
endfunction
w=rewrite(Irewrite0,'F',7); // 3 niveaux de re-ecriture a partir de F
[xs,ys]=calcul_segments(w,60,1);
rect=[min(xs),min(ys),max(xs),max(ys)]*1.1
xsetech(frect=rect);
xsegs(xs,ys);

// 3.0.3 Dessiner un arbre
function [xs,ys,ordre]=calcul_segments(w,delta,l)
    delta = delta*%pi/180;
    Rm = [cos(delta),-sin(delta);sin(delta),cos(delta)]
    Rp = [1,-1;-1,1].*Rm;
    L=length(w);
    stack=ones(5,L); // Matrice utilisee comme pile
    stc=1; // Premier indice utilisable dans la pile
    res=ones(5,L); // Matrice pour stocker les segments calcules
    count=1; // Premier indice utilisable pour stocker un segment
    pt=[0;0];
    ddir=[0;1];
    ordre=1; // Compteur d'arborescence
    for i=1:length(w)
        select part(w,i)
        case '[' then
            stack(:,stc)=[pt;ddir;ordre];stc=stc+1; // Il faut empiler les valeurs courantes
            ordre=ordre+1;
        case ']' then
            stc=stc-1;xx=stack(:,stc); // Il faut depiler les valeurs courantes
            pt=xx(1:2); ddir=xx(3:4); ordre=xx(5);
        case '+' then
            ddir= Rp*ddir; // Rotation +
        case '-' then
            ddir= Rm*ddir; // Rotation -
        else
            newpt=pt + l*ddir; // Calcul d'un nouveau point
            res(:,count)=[pt(1);newpt(1);pt(2);newpt(2);ordre];
            pt = newpt;
            count=count+1;
        end
    end
    xs=res(1:2,1:count-1);
    ys=res(3:4,1:count-1);
    ordre=res(5,1:count-1);
endfunction
function [w]=Irewrite1(v)
    w=strsubst(v,'F','F[+F]F[-F]F');
endfunction
scf();
w=rewrite(Irewrite1,'F',5);
[xs,ys,ordre]=calcul_segments(w,30,1);
rect=[min(xs),min(ys),max(xs),max(ys)]*1.1

xsetech(frect=rect);
xsegs(xs,ys); // Voir Figue 3.2

function []=dessiner(xs,ys,ordre)
    rect=[min(xs),min(ys),max(xs),max(ys)]*1.1
    xsetech(frect=rect); // On ¯xe l'echelle courante
    m=max(ordre);
    if m <> 1 then
        cmap=[((1:m).^2)/m;1:m;((1:m).^2)/m]'/m;
        xset("colormap",cmap) // On change de table de couleur
    end
    is=size(xs,'c');
    tm=max(ordre);
    for i=min(ordre):max(ordre)
        I=find(ordre==i); // On selectionne les segments d'ordre i
        xset("thickness",1*(tm-ordre(I(1)))); // Epaisseur  du trait fonction de l'ordre
        xsegs(xs(:,I),ys(:,I),i) // Les segments d'ordre i avec la couleur i
    end
endfunction
function [w]=Irewrite2(v)
    w=strsubst(v,'H','F-[[H]+H]+F[+FH]-H');
    w=strsubst(w,'F','FF');
endfunction
if ( %f ) then
    // TODO : fixme
  set('old_style','on'); // On selectionne l'ancien mode graphique
end
scf();
w=rewrite(Irewrite2,'H',5);
[xs,ys,ordre]=calcul_segments(w,40,1);
dessiner(xs,ys,ordre);

function [w]=Irewrite3(v)
    rew=['F[+F]','F[-F]','F[-F]F','F[+F]F']
    w="";
    for i=1:length(v)
        if part(v,i)== 'F' then
            w = w + rew(grand(1,'uin',1,4));
        else
            w = w +part(v,i)
        end
    end
endfunction
if ( %f ) then
    set('old_style','on'); // On selectionne l'ancien mode graphique
end
scf();
w=rewrite(Irewrite3,'F',9);
[xs,ys,ordre]=calcul_segments(w,32,1);
dessiner(xs,ys,ordre);
