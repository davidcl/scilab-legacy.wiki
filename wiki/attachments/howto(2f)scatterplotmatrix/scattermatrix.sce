// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function scattermatrix(varargin)
    // Create scatter plot matrix
    //
    // Calling Sequence
    //   scattermatrix(x)
    //   scattermatrix(x,key1,value1,key2,value2,...)
    //   scattermatrix(x,"xlabels",xlabels)
    //   scattermatrix(x,"ptsize",ptsize)
    //   scattermatrix(x,"valuelabels",valuelabels)
    //   scattermatrix(x,"symbol",symbol)
    //   scattermatrix(x,"histogram",histogram)
    //   scattermatrix(x,"nbclasses",nbclasses)
    //   scattermatrix(x,"histoYlabel",histoYlabel)
    //
    // Parameters
    //   x : a n-by-ninput matrix of doubles, the x datas
    //   xlabels : a ninput-by-1 matrix of strings, the x labels (default="")
    //   labelling : a 1-by-1 matrix of booleans, the labelling method (default=%t)
    //   ptsize : a 1-by-1 matrix of doubles, integer value, positive, the number pixels for the dots (default ptsize=2)
    //   valuelabels : a 1-by-1 matrix of booleans, set to true to print the x value labels (default=%t)
    //   symbol : a 1-by-1 matrix of strings, the point symbols (default="b.")
    //   histogram : a 1-by-1 matrix of booleans, set to true to print the histogram (default=%f)
    //   nbclasses : a 1-by-1 matrix of doubles, integer value, positive, the number of classes in the histogram (default nbclasses=10)
    //   histoYlabel : a 1-by-1 matrix of strings, the Y-label in the histogram (default histoYlabel="Frequency")
    //
    // Description
    // Plots a matrix of scatter plots representing the 
    // dependencies of X.
    // 
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "scattermatrix" , rhs , 1:9 )
    apifun_checklhs ( "scattermatrix" , lhs , 0:1 )
    //
    x = varargin ( 1 )
    //
    // 1. Set the defaults
    default.xlabels = []
    default.ptsize = 2
    default.valuelabels = %t
    default.symbol = "b."
    default.histogram = %f
    default.nbclasses = 10
    default.histoYlabel = "Frequency"
    //
    // 2. Manage (key,value) pairs
    options=apifun_keyvaluepairs (default,varargin(2:$))
    //
    // 3. Get parameters
    xlabels=options.xlabels
    ptsize = options.ptsize
    valuelabels = options.valuelabels
    symbol = options.symbol
    histogram = options.histogram
    nbclasses = options.nbclasses
    histoYlabel = options.histoYlabel
    //
    // Check Type
    apifun_checktype ( "scattermatrix" , x , "x" , 1 , "constant" )
    if (xlabels<>[]) then
        apifun_checktype ( "scattermatrix" , xlabels , "xlabels" , 2 , "string" )
    end
    apifun_checktype ( "scattermatrix" , ptsize , "ptsize" , 2 , "constant" )
    apifun_checktype ( "scattermatrix" , valuelabels , "valuelabels" , 2 , "boolean" )
    apifun_checktype ( "scattermatrix" , symbol , "symbol" , 2 , "string" )
    apifun_checktype ( "scattermatrix" , histogram , "histogram" , 2 , "boolean" )
    apifun_checktype ( "scattermatrix" , nbclasses , "nbclasses" , 2 , "constant" )
    apifun_checktype ( "scattermatrix" , histoYlabel , "histoYlabel" , 2 , "string" )
    //
    // Check Size
    n=size(x,"r");
    ninput=size(x,"c")
    //
    apifun_checkdims ( "scattermatrix" , x , "x" , 1 , [n ninput] )
    if (xlabels<>[]) then
        apifun_checkvector ( "scattermatrix" , xlabels , "xlabels" , 2 , ninput )
    end
    apifun_checkscalar ( "scattermatrix" , ptsize , "ptsize" , 2 )
    apifun_checkscalar ( "scattermatrix" , valuelabels , "valuelabels" , 2 )
    apifun_checkscalar ( "scattermatrix" , symbol , "symbol" , 2 )
    apifun_checkscalar ( "scattermatrix" , histogram , "histogram" , 2 )
    apifun_checkscalar ( "scattermatrix" , nbclasses , "nbclasses" , 2 )
    apifun_checkscalar ( "scattermatrix" , histoYlabel , "histoYlabel" , 2 )
    //
    // Check Content
    apifun_checkgreq ( "scattermatrix" , ptsize , "ptsize" , 2 , 1 )
    apifun_checkflint ( "scattermatrix" , ptsize , "ptsize" , 2 )
    apifun_checkgreq ( "scattermatrix" , nbclasses , "nbclasses" , 2 , 1 )
    apifun_checkflint ( "scattermatrix" , nbclasses , "nbclasses" , 2 )
    //
    scf();
    for j=1:ninput
        for i=1:ninput
            p=(j-1)*ninput + i;
            subplot(ninput,ninput,p);
            if (i==j) then
                if (histogram) then
                    histplot(nbclasses,x(:,i))
                    if (xlabels<>[]) then
                        xtitle("",xlabels(i),histoYlabel)
                    else
                        xtitle("","",histoYlabel)
                    end
                else
                    if (xlabels<>[]) then
                        xstring(0.5,0.5,xlabels(i))
                    end
                end
            else
                plot(x(:,i),x(:,j),symbol);
                e=gce();
                if (~valuelabels) then
                    e.parent.auto_ticks(1:3)="off";
                end
                e.children.mark_size=ptsize;
            end
        end
    end
endfunction

if (%f) then
    m=1000;
    x1=grand(m,1,"def");
    x2=grand(m,1,"def");
    x3=grand(m,1,"def");
    y1=2*x1.*x2+x3;
    y2=-3*x1+x2.^2-2*x3;
    y3=sin(x1)-3*x2+3*x3;
    y=[y1,y2,y3];
    //
    ylabels=["Y1","Y2","Y3"];
    // No labels
    scattermatrix(y);
    // With labels
    scattermatrix(y,"xlabels",ylabels);
    // With labels, without value labels
    scattermatrix(y,"xlabels",ylabels,"valuelabels",%f);
    // With labels, without value labels, with red circles
    scattermatrix(y,"xlabels",ylabels,"valuelabels",%f,..
    "symbol","ro");
    // With labels, without value labels, with red dots, 
    // with symbols of size 1
    scattermatrix(y,"xlabels",ylabels,"valuelabels",%f,..
    "symbol","r.","ptsize",1);
    // With the histogram
    scattermatrix(y,"histogram",%t);
    // With the histogram, and the labels
    scattermatrix(y,"histogram",%t,"xlabels",ylabels);
end
